const i18nModule = require('vue-i18n');

/* module.exports = {
    definition: function(){
        console.log('hello!');

        this.hello = function(){
            console.log('HELLO from class!');
        }
    }
} */

module.exports = class I18nClass{
    _global = {}
    _local = {}
    _messages = {};

    constructor(messages, sharedMessages) {
        this._global = i18nModule.useI18n({ useScope: "global" })
        this._local = i18nModule.useI18n({
            messages,
            sharedMessages: sharedMessages ? sharedMessages : {}
        });
        this._messages = messages;
    }

    global() {
        return this._global
    }

    local() {
        return this._local
    }

    globalFirst(key, replacements) {
        return this._global.te(key, replacements) ? this._global.t(key, replacements) : this._local.t(key, replacements)
    }

    gf(key, replacements){
        return this.globalFirst(key, replacements)
    }

    getMessages(){
        return this._messages;
    }
}

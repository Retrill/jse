module.exports = {
    beforeMount: function(el, binding){
        el.clickOutsideEvent = event => {
            if (!(el == event.target || el.contains(event.target))) {
                binding.value();
            }
        };
        document.addEventListener("click", el.clickOutsideEvent);
    },
    unmounted: function(el){
        document.removeEventListener("click", el.clickOutsideEvent);
    }
}

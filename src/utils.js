module.exports = {
    getFromLocalStorage: function(namespace, key, defaultValue) {
        const _this = this;

        if (namespace && !key) {
            return function(key, defaultValue) {
                // let memorizedPrefix = prefix;
                return _this.getFromLocalStorage(namespace, key, defaultValue);
            }
        }
        let value = localStorage.getItem(namespace + key);
        if( !value ){
            return defaultValue;
        }

        value = JSON.parse(localStorage.getItem(namespace + key));
        return value ? value : defaultValue;
    },
    saveToLocalStorage: function(namespace, key, value) {
        const _this = this;

        if (namespace && !key) {
            return function(key, value) {
                return _this.saveToLocalStorage(namespace, key, value);
            }
        }

        if (key && (typeof value === 'undefined')) {
            return function(value) {
                return _this.saveToLocalStorage(namespace, key, value);
            }
        }
        localStorage.setItem(namespace + key, JSON.stringify(value));
        return value;
    }
}

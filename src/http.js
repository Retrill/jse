module.exports = {
    post: function(endpoint, data, config) {
        return this.request('post', endpoint, data, config);
    },
    get: function(endpoint, data, config) {
        return this.request('get', endpoint, data, config);
    },
    request: (method, endpoint, data, config) => new Promise((resolve, reject) => {
        axios[method](endpoint, (method == 'get' && !('params' in data)) ? {'params': data} : data, config)
        .then((response) => {
            // console.log('axios response: ', response);
            try {
                let payload = {};
                let responseErrors = [];
                if (typeof response.data == 'object') {
                    payload = response.data.data || response.data.result;
                    responseErrors = ('errors' in response.data) ? response.data.errors : responseErrors;
                }else{
                    let responseJson = JSON.parse(response.data);
                    payload = responseJson.data;
                    responseErrors = ('errors' in responseJson) ? responseJson.errors : responseErrors;
                }

                if(responseErrors.length > 0){
                    reject({status: response.status, errors: responseErrors});
                }else{
                    resolve(payload);
                }
            } catch (errorText) {
                let msg = "unexpected non-json ajax response: ";
                console.error(msg, response.data);
                console.log(errorText);
                reject({status: response.status, errors: [errorText]});
            }
        })
        .catch(function (error) {
            // console.log('axios error? ', error);
            if (error.response) {
                // Request made and server responded
                let msg = "Ajax response error! status code = " + error.response.status;
                console.error(msg + "; ", error.response.data);
                console.log("response headers: ", error.response.headers);
                console.log("rejecting with status: ", );
                reject({status: error.response.status, errors: [msg]});
            } else if (error.request) {
                let msg = "The request was made but no response was received";
                console.error(msg + ": ", error.request);
                reject({status: 0, errors: [msg]}, 0);
            } else {
                let msg = "Something happened in setting up the request that triggered an Error";
                console.error(msg + ": ", error.message);
                reject({status: 0, errors: [msg]});
            }
        });
    })
}

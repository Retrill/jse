const http = require('./src/http.js');
const utils = require('./src/utils.js');
const I18nClass = require('./src/i18n.js');
const ClickOutside = require('./src/directives/click-outside.js');
// import http from './src/http.js';

module.exports = { http, utils, I18nClass, ClickOutside }
